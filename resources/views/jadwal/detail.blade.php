@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <form method="post"
            action="{{ route('peminjamanUpdate', ['id' => $edit->id]) }}"
            class="form-send m-form m-form--fit m-form--label-align-right"
            enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Detail Jadwal
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label>Nama Akun</label>
                            <input type="text" name="nama_akun" value="{{ $edit->nama_akun }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="nama_kegiatan" value="{{ $edit->nama_kegiatan }}" class="form-control m-input " disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Kapasitas</label>
                            <input type="text" name="kapasitas" value="{{ $edit->kapasitas }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Deskripsi</label>
                            <input type="text" name="deskripsi" value="{{ $edit->deskripsi }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Pinjam</label>
                            <input type="text" name="tanggal_pinjam" value="{{ $edit->tanggal_pinjam }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Kembali</label>
                            <input type="text" name="tanggal_kembali" value="{{ $edit->tanggal_kembali }}" class="form-control m-input" disabled>
                        </div>
                        <div class="form-group m-form__group">
                            <label>Link Zoom</label>
                            <input type="text" name="catatan" value="{{ $edit->catatan }}" class="form-control m-input" disabled>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
@endsection