@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Daftar Jadwal
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m_datatable" id="local_data">
                        <div class="table-responsive">
                            <table class="akses-list table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th width="20">Nama Akun Zoom</th>
                                        <th width="20">Nama Kegiatan</th>
                                        <th width="20">Deskripsi</th>
                                        <th width="20">Kapasitas</th>
                                        <th width="20">Tanggal Pinjam</th>
                                        <th width="20">Tanggal Kembali</th>
                                        <th width="20">Status</th>
                                        <th width="20">Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data_jadwal as $data)
                                        @if($data->status == ('Approved'))
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nama_akun }}</td>
                                            <td>{{ $data->nama_kegiatan }}</td>
                                            <td>{{ $data->deskripsi }}</td>
                                            <td>{{ $data->kapasitas }}</td>
                                            <td>{{ $data->tanggal_pinjam }}</td>
                                            <td>{{ $data->tanggal_kembali }}</td>
                                            <td>
                                                <button class="btn m-btn--pill btn-success btn-sm m-btn m-btn--custom">
                                                {{ $data->status }}
                                                </button>
                                            </td>
                                            <td>
                                                <a href="/tampilDetail/{{ $data->id }}">
                                                    <button type="button" class="btn m-btn--pill btn-info btn-sm m-btn m-btn--custom" }}>
                                                      Detail 
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection