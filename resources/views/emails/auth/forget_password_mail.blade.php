@component('mail::message')

Hello {{ $username }}.

@component('mail::button', ['url' => route('ResetPassword', $reset_code)])
Click here to reset your password
@endcomponent
<p>Or copy & paste the following link to your browser</p>
<p><a href="{{ route('ResetPassword', $reset_code) }}">{{ route('ResetPassword', $reset_code) }}</a></p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
