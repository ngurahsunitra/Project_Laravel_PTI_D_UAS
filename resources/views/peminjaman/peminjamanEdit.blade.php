@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        Data Peminjaman
                    </h3>
                </div>
            </div>
        </div>

        <form method="post"
            action="{{ route('peminjamanUpdate', ['id' => $edit->id]) }}"
            class="form-send m-form m-form--fit m-form--label-align-right"
            enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Edit Peminjaman
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label>Status</label>
                            <select name="status" id="exampleSelect1" class="@error('status') is-invalid @enderror  form-control m-input m-input--square" required value="{{ old('catatan') }}">
                                <option value="Approved" {{ $edit->status == 'Approved' ? 'selected':'' }}>Approved</option>
                                <option value="Rejected" {{ $edit->status == 'Rejected' ? 'selected':'' }}>Rejected</option>
                                @if ($edit->status != 'Finished')
                                    <option value="Cancelled" {{ $edit->status == 'Cancelled' ? 'selected':'' }}>Cancelled</option>
                                @endif
                                <option value="Finished" {{ $edit->status == 'Finished' ? 'selected':'' }}>Finished</option>
                            </select>
                            @error('status')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                        <div class="form-group m-form__group">
                            <label>Catatan</label>
                            <textarea name="catatan" class="@error('catatan') is-invalid @enderror form-control m-input">{{ $edit->catatan }}</textarea>
                            @error('catataan')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>

                    </div>
                    <div class="m-portlet akses-list">
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions text-center">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('peminjamanList') }}" class="btn btn-secondary">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
@endsection