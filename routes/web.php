<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZoomController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\ForgotPassController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate'])->name('authenticate')->middleware('guest');

Route::get('/forgot-password', [ForgotPassController::class, 'index'])->name('getForgetPassword')->middleware('guest');
Route::post('/forgot-password', [ForgotPassController::class, 'create'])->name('forgotpassword')->middleware('guest');
Route::get('/reset-password/{reset_code}', [ForgotPassController::class, 'getResetPassword'])->name('ResetPassword')->middleware('guest');
Route::post('/reset-password/{reset_code}', [ForgotPassController::class, 'postResetPassword'])->name('postResetPassword')->middleware('guest');

Route::post('/logout', [LoginController::class, 'logout'])->name('logout')->middleware('UserLevel');

Route::get('/register', [RegisterController::class, 'index'])->name('register')->middleware('guest');
Route::post('/register', [RegisterController::class, 'store'])->name('registerStore');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('UserLevel');
Route::get('/stafDashboard', [DashboardController::class, 'stafindex'])->name('stafdashboard')->middleware('UserLevel');

Route::get('/tampilJadwal', [JadwalController::class, 'index'])->name('jadwalList')->middleware('UserLevel');
Route::get('/tampilDetail/{id}', [JadwalController::class, 'detail'])->name('detailList')->middleware('UserLevel');

Route::get('/tampilZoom', [ZoomController::class, 'index'])->name('zoomList')->middleware('UserLevel');
Route::get('/createZoom', [ZoomController::class, 'create'])->name('zoomCreate')->middleware('UserLevel');
Route::post('/insertZoom', [ZoomController::class, 'insert'])->name('zoomInsert')->middleware('UserLevel');
Route::get('/editZoom/{id}', [ZoomController::class, 'edit'])->name('zoomEdit')->middleware('UserLevel');
Route::post('/updateZoom/{id}', [ZoomController::class, 'update'])->name('zoomUpdate')->middleware('UserLevel');
Route::get('/deleteZoom/{id}', [ZoomController::class, 'delete'])->name('zoomDelete')->middleware('UserLevel');

Route::get('/tampilPeminjaman', [PeminjamanController::class, 'index'])->name('peminjamanList')->middleware('UserLevel');
Route::get('/createPeminjaman', [PeminjamanController::class, 'create'])->name('peminjamanCreate')->middleware('UserLevel');
Route::post('/insertPeminjaman', [PeminjamanController::class, 'insert'])->name('peminjamanInsert')->middleware('UserLevel');
Route::get('/editPeminjaman/{id}', [PeminjamanController::class, 'edit'])->name('peminjamanEdit')->middleware('UserLevel');
Route::post('/updatePeminjaman/{id}', [PeminjamanController::class, 'update'])->name('peminjamanUpdate')->middleware('UserLevel');
Route::get('/deletePeminjaman/{id}', [PeminjamanController::class, 'delete'])->name('peminjamanDelete')->middleware('UserLevel');
