<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\peminjaman;
use App\Models\zoom;

class JadwalController extends Controller
{
    public function index()
    {

        $peminjaman = peminjaman
            ::join('zoom', 'peminjaman.id_zoom', '=', 'zoom.id')
            ->select('zoom.*', 'peminjaman.*')
            ->get();

        return view('jadwal.jadwal', ['data_jadwal' => $peminjaman]);
    }

    public function detail($id)
    {
        $peminjaman = peminjaman::where('peminjaman.id', $id)
            ->join('zoom', 'peminjaman.id_zoom', '=', 'zoom.id')
            ->select('zoom.*', 'peminjaman.*')
            ->first();


        $data = [
            'edit' => $peminjaman,
        ];

        return view('jadwal.detail', $data);
    }
}
