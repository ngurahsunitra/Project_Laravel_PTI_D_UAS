<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\passwordReset;
use Illuminate\Support\Str;
use App\Mail\ForgetPasswordMail;
use Illuminate\Support\Facades\Mail;

class ForgotPassController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        return view('forgotpassword.forgotPassword');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|email:dns',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return back()->with('error', 'User not registered');
        } else {
            $reset_code = Str::random(100);
            passwordReset::create([
                'user_id' => $user->id,
                'reset_code' => $reset_code
            ]);

            Mail::to($user->email)->send(new ForgetPasswordMail($user->name, $reset_code));

            return redirect()->back()->with('success', 'We have sent you a password reset link. Please check your email.');
        }
    }
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function getResetPassword($reset_code)
    {
        $password_reset_data = passwordReset::where('reset_code', $reset_code)->first();

        if (!$password_reset_data || Carbon::now()->subMinutes(100) > $password_reset_data->created_at) {
            return redirect()->route('getForgetPassword')->with('error', 'Invalid password reset link or link is expired.');
        } else {
            return view('forgotpassword.resetPassword', compact('reset_code'));
        }
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function postResetPassword($reset_code, Request $request)
    {
        $password_reset_data = passwordReset::where('reset_code', $reset_code)->first();

        if (!$password_reset_data || Carbon::now()->subMinutes(10) > $password_reset_data->created_at) {
            return redirect()->route('getForgetPassword')->with('error', 'Invalid password reset link or link is expired.');
        } else {
            $request->validate([
                'email' => 'required|email:dns',
                'password' => 'required|min:6',
            ]);

            $user = User::find($password_reset_data->user_id);

            if ($user->email != $request->email) {
                return redirect()->back()->with('error', 'Enter correct email!');
            } else {
                $password_reset_data->delete();
                $user->update([
                    'password' => bcrypt($request->password)
                ]);
                return redirect()->route('login')->with('sukses', 'Your password has been changed!');
            }
        }
    }
}
