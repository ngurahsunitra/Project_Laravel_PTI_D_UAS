<?php

namespace App\Http\Controllers;

use App\Models\zoom;
use App\Models\peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class peminjamanController extends Controller
{
    public function index()
    {
        $peminjaman = peminjaman
            ::join('zoom', 'peminjaman.id_zoom', '=', 'zoom.id')
            ->select('zoom.*', 'peminjaman.*')
            ->get();


        return view('peminjaman.peminjamanList', ['data_peminjaman' => $peminjaman]);
    }

    public function create()
    {
        $zoom = zoom::all();
        return view('peminjaman.peminjamanCreate', ['zoom' => $zoom]);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'id_zoom' => 'required',
            'nama_kegiatan' => 'required',
            'deskripsi' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]);


        // CEK TANGGAL AKUN ZOOM
        $tgl_pinjam = peminjaman::where('id_zoom', $request->id_zoom)->whereBetween('tanggal_pinjam', [$request->tanggal_pinjam, $request->tanggal_kembali])->first();

        // KONDISI JIKA JADWAL SUDAH TERDAFTAR
        if ($tgl_pinjam != null) {
            // MASUKKAN ULANG JADWAL
            return redirect('/createPeminjaman')->with('gagal', 'Jadwal Tidak Tersedia!');
        }

        $peminjaman = new peminjaman();
        $peminjaman->id_zoom = $request->id_zoom;
        $peminjaman->nama_kegiatan = $request->nama_kegiatan;
        $peminjaman->deskripsi = $request->deskripsi;
        $peminjaman->tanggal_pinjam = ($request->tanggal_pinjam);
        $peminjaman->tanggal_kembali = ($request->tanggal_kembali);

        $peminjaman->save();

        return redirect('/tampilPeminjaman')->with('sukses', 'Data berhasil tersimpan!');
    }

    public function edit($id)
    {
        $peminjaman = peminjaman::where('id', $id)->first();

        $data = [
            'edit' => $peminjaman,
        ];

        return view('peminjaman.peminjamanEdit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'catatan' => 'required',
        ]);

        $peminjaman = peminjaman::find($id);
        $peminjaman->status = $request->status;
        $peminjaman->catatan = $request->catatan;

        $peminjaman->save();

        if ($peminjaman)
            return redirect('/tampilPeminjaman')->with('sukses', 'Status berhasil diedit!');
        else
            return redirect('/tampilPeminjaman')->with('gagal', 'Status gagal diedit!');;
    }

    public function delete($id)
    {
        $peminjaman = peminjaman::find($id);
        $status = peminjaman
            ::select('peminjaman.*')
            ->where('peminjaman.id', $id)
            ->where('peminjaman.status', 'Approved')
            ->count();

        if ($peminjaman != null) {
            if ($status) {
                return redirect('/tampilPeminjaman')->with('gagal', 'Status Peminjaman Approved, Tidak dapat menghapus request peminjaman');
            } else {
                $peminjaman->delete();
                return redirect('/tampilPeminjaman')->with('sukses', 'Data berhasil dihapus!');;
            }
        }
    }
}
